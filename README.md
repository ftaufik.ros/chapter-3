# Challange Chapter 3

### Tentang Project :

- Challange Chapter 3
- Project ini dibuat untuk memenuhi persyaratan kelulusan pada chapter 3 Binar Bootcamp Full-Stack Web
- Berlatar belakang permainan tradisional yaitu "suit" (batu, gunting, kertas)

### Teknologi yang digunakan :

- WSL 2: Ubuntu-20.04
- Git
- HTML
- CSS
- Bootstrap 5
